from core.baseplugin import BasePlugin
from core.menuitem import MenuItem
from core.page import Page
from core.widget import Widget
from core.config import Config
from plugins.ldap.utils import gen_password, LdapWrap, CsvBuilder
from core.docker_manager import DockerManager
import socket
from bottle import request
from core.helpers.flash import Flash
import logging
import time
import os
import uuid


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem("ldap", 'fas fa-address-book', "/plugins/ldap/params"))
        self.register_menu_item(MenuItem(self.strings.users, 'fas fa-user', "/plugins/ldap/users"))
        self.register_menu_item(MenuItem(self.strings.groups, 'fas fa-users', "/plugins/ldap/groups"))

        self.register_page(Page('params', Page.METHOD_GET, self.action_params))
        self.register_page(Page('params', Page.METHOD_POST, self.action_params_process))
        self.register_page(Page('users', Page.METHOD_GET, self.action_users))
        self.register_page(Page('users', Page.METHOD_POST, self.action_users_process))
        self.register_page(Page('groups', Page.METHOD_GET, self.action_groups))
        self.register_page(Page('groups', Page.METHOD_POST, self.action_groups_process))

        self.register_widget(Widget(self.strings.users, self.wid))
        self.register_widget(Widget(self.strings.groups, self.wid_group))

        self.dns_plugin = ("dns" in Config().get("plugins", []))

        self.ldap = LdapWrap(self.strings, Config().get("worker_ip"), "cn=admin",
                             self.config.get("password"), self.config.get("base_dn"))

    def wid(self):
        return self.render('widgets/users', {"nb_users": len(self.ldap.get_users())})

    def wid_group(self):
        return self.render('widgets/groups', {"nb_groups": len(self.ldap.get_groups())})

    def action_params(self):
        return self.render("params")

    def action_params_process(self):
        request.body.read()
        upload = request.files.get('file_users')
        if upload:
            name, ext = os.path.splitext(upload.filename)
            if ext == ".csv":
                file = "{0}/{1}.csv".format(Config().get("data_location"), uuid.uuid4())
                upload.save(file)
                csv = CsvBuilder(file)
                strout = ""
                if csv.get_type() == ['\ufeff#TYPE Selected.Microsoft.ActiveDirectory.Management.ADUser']:
                    for user in csv.get_object_list():
                        password = gen_password(8)
                        result = self.ldap.add_user(user['SamAccountName'], user['GivenName'],
                                                    user['Surname'], user['Name'],
                                                    user["UserPrincipalName"], password)
                        if result:
                            self.ldap.add_user_to_group(user['SamAccountName'], "users")
                            g1 = user['Members of'].split(' CN=')
                            for g2 in g1:
                                g2 = g2.replace('CN=', '')
                                g3 = g2.split(',')
                                self.ldap.add_user_to_group(user['SamAccountName'], g3[0])
                            strout += self.strings.user_added.format(user['SamAccountName'], password)
                        else:
                            strout += self.strings.error_add_user.format(user['SamAccountName'])
                            logging.error("{0} ({1})".format(self.strings.error_add_user.format(user['SamAccountName']),
                                                             self.ldap.getresult()))
                        strout += "<br>"
                    Flash.set(Flash.TYPE_INFO, strout)
                os.remove(file)
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.extension_error)

        group_upload = request.files.get('file_groups')
        if group_upload:
            name, ext = os.path.splitext(group_upload.filename)
            if ext == ".csv":
                file = "{0}/{1}.csv".format(Config().get("data_location"), uuid.uuid4())
                group_upload.save(file)
                csv = CsvBuilder(file)
                strout = ""
                if csv.get_type() == ['\ufeff#TYPE Microsoft.ActiveDirectory.Management.ADGroup']:
                    for group in csv.get_object_list():
                        if self.ldap.add_group(group['Name']):
                            strout += self.strings.group_added.format(group['Name'])
                        else:
                            strout += self.strings.error_add_group.format(group['Name'])
                            logging.error(
                                "{0} ({1})".format(self.strings.error_add_group.format(group['Name']),
                                                   self.ldap.getresult()))
                        strout += "<br>"
                    Flash.set(Flash.TYPE_INFO, strout)
                os.remove(file)
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.extension_error)

        return self.action_params()

    def action_users(self):
        return self.render("users", {
            "users": self.ldap.get_users()
        })

    def action_users_process(self):
        request.body.read()
        if 'first_name' in request.forms.keys() \
                and 'last_name' in request.forms.keys() \
                and 'cn' in request.forms.keys() \
                and 'display_name' in request.forms.keys() \
                and 'email' in request.forms.keys() \
                and 'user' not in request.forms.keys():
            password = gen_password(8)
            result = self.ldap.add_user(request.forms.cn, request.forms.first_name,
                                        request.forms.last_name, request.forms.display_name,
                                        request.forms.email, password)
            if result:
                self.ldap.add_user_to_group(request.forms.cn, "users")
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_added.format(request.forms.cn, password))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_add_user.format(request.forms.cn))
                logging.error("{0} ({1})".format(self.strings.error_add_user.format(request.forms.cn), self.ldap.getresult()))

        if 'user' in request.forms.keys() \
                and 'first_name' in request.forms.keys() \
                and 'last_name' in request.forms.keys() \
                and 'cn' in request.forms.keys() \
                and 'display_name' in request.forms.keys() \
                and 'email' in request.forms.keys():
            result = self.ldap.update_user(request.forms.user, request.forms.first_name,
                                           request.forms.last_name, request.forms.display_name, request.forms.email)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_edited.format(request.forms.user))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_edit_user.format(request.forms.user))
                logging.error("{0} ({1})".format(self.strings.error_edit_user.format(request.forms.user), self.ldap.getresult()))

        if 'delete_user' in request.forms.keys():
            groups = self.ldap.get_groups()
            for group in groups:
                if "uid={0},ou=users,{1}".format(request.forms.delete_user, self.ldap.base_dn) in group.member:
                    self.ldap.remove_user_from_group(request.forms.delete_user, group.cn)
            result = self.ldap.delete_user(request.forms.delete_user)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_deleted.format(request.forms.delete_user))
            else:
                Flash.set(Flash.TYPE_ERROR,
                          self.strings.error_delete_user.format(request.forms.delete_user))
                logging.error("{0} ({1})".format(
                    self.strings.error_delete_user.format(request.forms.delete_user), self.ldap.getresult()))

        if 'reset_password' in request.forms.keys():
            password = gen_password(8)
            result = self.ldap.update_user_password(request.forms.reset_password, password)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_password_reset.format(request.forms.reset_password, password))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_reset_user_password.format(request.forms.reset_password))
                logging.error("{0} ({1})".format(self.strings.error_reset_user_password.format(request.forms.reset_password), self.ldap.getresult()))

        return self.action_users()

    def action_groups(self, group_opened=None, msg_add=None):
        r_groups = self.ldap.get_groups()
        r_users = self.ldap.get_users()
        groups = []
        for r_group in r_groups:
            group = {
                'name': r_group.cn,
                'users': [],
                'available_users': []
            }
            for dn in r_group.member:
                if dn != "cn=admin,{0}".format(self.ldap.base_dn):
                    group['users'].append(self.ldap.get_user(dn))

            for user in r_users:
                if "uid={0},ou=users,{1}".format(user.uid, self.ldap.base_dn) not in r_group.member:
                    group['available_users'].append({'text': user.displayName, 'value': user.uid})

            groups.append(group)

        return self.render("groups", {
            "group_opened": group_opened,
            "msg_add": msg_add,
            "groups": groups,
            "users": [
                {'text': 'Charly Hue', 'value': 'chue'}, {'text': 'Adrien Bedin', 'value': 'abedin'}
            ]
        })

    def action_groups_process(self):
        request.body.read()
        group_opened = None
        if 'group_name' in request.forms.keys():
            if self.ldap.add_group(request.forms.group_name):
                Flash.set(Flash.TYPE_SUCCESS, self.strings.group_added.format(request.forms.group_name))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_add_group.format(request.forms.group_name))
                logging.error("{0} ({1})".format(self.strings.error_add_group.format(request.forms.group_name),
                                                 self.ldap.getresult()))
        if 'group' in request.forms.keys() and 'user' in request.forms.keys():
            group_opened = request.forms.group
            result = self.ldap.add_user_to_group(request.forms.user, request.forms.group)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_added_to_group.format(request.forms.user, request.forms.group))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_user_add_to_group.format(request.forms.user, request.forms.group))
                logging.error("{0} ({1})".format(self.strings.error_user_add_to_group.format(request.forms.user, request.forms.group),
                                                 self.ldap.getresult()))

        if 'delete_user_group' in request.forms.keys() and 'delete_user' in request.forms.keys():
            group_opened = request.forms.delete_user_group
            result = self.ldap.remove_user_from_group(request.forms.delete_user, request.forms.delete_user_group)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.user_removed_from_group.format(request.forms.delete_user,
                                                                                          request.forms.delete_user_group))
            else:
                Flash.set(Flash.TYPE_ERROR, self.strings.error_remove_user_from_group.format(request.forms.delete_user,
                                                                                             request.forms.delete_user_group))
                logging.error("{0} ({1})".format(
                    self.strings.error_remove_user_from_group.format(request.forms.delete_user,
                                                                     request.forms.delete_user_group),
                    self.ldap.getresult()))

        if 'delete_group' in request.forms.keys():
            result = self.ldap.delete_group(request.forms.delete_group)
            if result:
                Flash.set(Flash.TYPE_SUCCESS, self.strings.group_deleted.format(request.forms.delete_group))
            else:
                Flash.set(Flash.TYPE_ERROR,
                          self.strings.error_delete_group.format(request.forms.delete_group))
                logging.error("{0} ({1})".format(
                    self.strings.error_delete_group.format(request.forms.delete_group), self.ldap.getresult()))

        return self.action_groups(group_opened=group_opened)

    def deploy(self):

        if super().deploy():
            retry = True
            retry_count = 0
            while retry or retry_count > 100:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex((Config().get("worker_ip"), 389))
                if result == 0:
                    logging.debug("Ldap stack deployed")
                    retry = False
                logging.debug("Ldap not ready, i will retry...")
                retry_count += 1
                sock.close()
                time.sleep(0.5)

            self.config.set("base_dn", "dc=" + ",dc=".join(self.config.get('domain').split('.')))

            self.ldap = LdapWrap(self.strings, Config().get("worker_ip"), "cn=admin",
                                 self.config.get("password"),
                                 self.config.get("base_dn"))
            self.ldap.add_ou("users")
            self.ldap.add_ou("groups")
            self.ldap.add_group("admins")
            self.ldap.add_group("users")

            ldap_config = {
                'server': Config().get("worker_ip"),
                'user': "cn=admin," + self.config.get("base_dn"),
                'search': "uid={0},ou=users," + self.config.get("base_dn"),
                'admin_group': "cn=admins,ou=groups," + self.config.get("base_dn"),
                "password": self.config.get("password")
            }
            Config().set('ldap', ldap_config)

            return True
        else:
            return False

    def install(self):
        if self.config.get("domain", None) is not None and self.config.get("password", None) is not None:
            if not DockerManager().has_secret("simtio_ldap_slapd_password"):
                DockerManager().set_secret("simtio_ldap_slapd_password", self.config.get("password", ""))
            self.deploy()
        else:
            self.config.set("password", gen_password(16))
            DockerManager().set_secret("simtio_ldap_slapd_password", self.config.get("password", ""))
            if self.dns_plugin:
                self.config.set("domain", Config().get("plugins")['dns']['dns_domain'])
                self.deploy()

    def get_groups(self):
        return self.ldap.get_groups()
