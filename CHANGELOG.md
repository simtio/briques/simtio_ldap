# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.2] - 2020-04-15
### Fixed
- hard coded base dn

## [1.1.1] - 2020-03-29
### Added
- MemberOf feature

## [1.1.0] - 2020-02-21
### Added
- Action log messages

## [1.0.4] - 2019-12-20
### Fixed
- Secret creation prevent update

## [1.0.3] - 2019-12-19
### Added
- get_groups method

## [1.0.2] - 2019-12-19
### Added
- CI
### Changed
- dynamic version in docker-compose
- Fix dockerfile FROM tag
