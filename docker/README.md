# SIMTIO - LDAP

## Usage

```
docker run -d -p 389:389 -p 636:636 -v ldap-conf:/etc/ldap -v ldap-data:/var/lib/data -e SLAPD_PASSWORD=mysecretpassword -e SLAPD_DOMAIN=ldap.example.org --name=simtio_ldap registry.gitlab.com/simtio/briques/simtio_ldap:latest
```

The volumes are used to have persistent storage, theses are the ones you need :
* `ldap-conf:/etc/ldap`
* `ldap-data:/var/lib/ldap`

## Configuring

After the first start of the image (and the initial configuration), these
envirnonment variables are not evaluated again (see the
`SLAPD_FORCE_RECONFIGURE` option).

* `SLAPD_PASSWORD` (required) - sets the password for the `admin` user.
* `SLAPD_DOMAIN` (required) - sets the DC (Domain component) parts. E.g. if one sets
it to `ldap.example.org`, the generated base DC parts would be `...,dc=ldap,dc=example,dc=org`.
* `SLAPD_ORGANIZATION` (defaults to $SLAPD_DOMAIN) - represents the human readable
company name (e.g. `Example Inc.`).
* `SLAPD_CONFIG_PASSWORD` - allows password protected access to the `dn=config`
branch. This helps to reconfigure the server without interruption (read the
[official documentation](http://www.openldap.org/doc/admin24/guide.html#Configuring%20slapd)).
* `SLAPD_ADDITIONAL_SCHEMAS` - loads additional schemas provided in the `slapd`
package that are not installed using the environment variable with comma-separated
enties. As of writing these instructions, there are the following additional schemas
available: `collective`, `corba`, `duaconf`, `dyngroup`, `java`, `misc`, `openldap`,
`pmi` and `ppolicy`.
* `SLAPD_ADDITIONAL_MODULES` - comma-separated list of modules to load. It will try
to run `.ldif` files with a corresponsing name from the `module` directory.
Currently only `memberof` and `ppolicy` are avaliable.

* `SLAPD_FORCE_RECONFIGURE` - (defaults to false)  Used if one needs to reconfigure
the `slapd` service after the image has been initialized.  Set this value to `true`
to reconfigure the image.

## Dev
```
docker build -t registry.gitlab.com/simtio/briques/simtio_ldap:latest -t registry.gitlab.com/simtio/briques/simtio_ldap:0.x .
docker push registry.gitlab.com/simtio/briques/simtio_ldap:latest && docker push registry.gitlab.com/simtio/briques/simtio_ldap:0.x

docker run --rm -d -p 389:389 -p 636:636 -v ldap-conf:/etc/ldap -v ldap-data:/var/lib/data -e SLAPD_PASSWORD=mysecretpassword -e SLAPD_DOMAIN=ldap.example.org --name=simtio-ldap registry.gitlab.com/simtio/briques/simtio_ldap
```

## Sources
- https://github.com/dinkel/docker-openldap
