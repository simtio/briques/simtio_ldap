var $ = require('jquery');
var Mustache = require('mustache');


$('.tag .delete').on('click', process_remove_user);

$('.add-user-to-group').on("submit", function(e) {
    e.preventDefault();
    var data = $($(this),":input").serializeArray();
    var entry = {};
    $.each(data, function(k, v) {
        entry[v.name] = v.value;
    })
    add_user_badge(entry.group, entry.user)
    //console.log($(this).find('input.group').val())
});

function add_user_badge(group, user) {
    var tpl = `
    <span class="tag is-info">
      {{ name }}
    <button class="delete is-small tag-delete"></button>
    </span>`
    console.log(Mustache.render(tpl, {name: user}))
    $("#group-" + group).find(".user_list").append(Mustache.render(tpl, {name: user}))

    $("#group-" + group).find(".user_list").find(".tag").find(".delete").last().on('click', process_remove_user);
}

function process_remove_user() {
    $(this).parent().remove();
}