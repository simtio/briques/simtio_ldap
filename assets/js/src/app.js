var $ = require('jquery');

$(".collapse").append('<span class="is-pulled-right collapse-button" data-open="false"><i class="fa fa-fw fa-chevron-down"></i></span>');

$(".collapse").on('click', function(event) {
    toggle($(this))
})

$('.start-opened').each(function() {
    toggle($(this));
})


function toggle(elem) {
    elem.next(".content").slideToggle(200);
    if (elem.find(".collapse-button").attr('data-open') == 'false') {
        elem.find(".collapse-button").html('<i class="fa fa-fw fa-chevron-up"></i>')
        elem.find(".collapse-button").attr('data-open', "true")
    }
    else {
        elem.find(".collapse-button").html('<i class="fa fa-fw fa-chevron-down"></i>')
        elem.find(".collapse-button").attr('data-open', "false")
    }
}