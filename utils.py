import string
import random
from ldap3 import Server, Connection, MODIFY_REPLACE, MODIFY_ADD, MODIFY_DELETE, set_config_parameter
from ldap3.core.exceptions import LDAPSocketOpenError
from core.helpers.flash import Flash
import logging
import csv
from core.actionlogger import ActionLogger


def gen_password(length):
    """Generate a random password """
    choices = string.ascii_letters + string.digits
    return ''.join(random.choice(choices) for i in range(length))


class LdapWrap:
    def __init__(self, strings, server, user, password, base_dn):
        set_config_parameter('DEFAULT_ENCODING', 'UTF-8')
        self.server = Server(server)
        self.conn = Connection(self.server, user="{0},{1}".format(user, base_dn), password=password)
        self.base_dn = base_dn
        self.strings = strings

    def bind(self):
        self.conn.bind()

    def unbind(self):
        self.conn.unbind()

    def getresult(self):
        return self.conn.result

    def add(self, *args):
        self.bind()
        r = self.conn.add(*args)
        self.unbind()
        return r

    def search(self, *args, attributes=None):
        self.bind()
        self.conn.search(*args, attributes=attributes)
        r = self.conn.entries
        self.unbind()
        return r

    def update(self, dn, updates):
        self.bind()
        r = self.conn.modify(dn, updates)
        self.unbind()
        return r

    def delete(self, dn):
        self.bind()
        r = self.conn.delete(dn)
        self.unbind()
        return r

    def add_ou(self, name):
        return self.add('ou={0},{1}'.format(name, self.base_dn), "organizationalUnit")

    def add_group(self, name):
        ActionLogger().info(self.strings.log_add_group.format(name))
        return self.add('cn={0},ou=groups,{1}'.format(name, self.base_dn), "groupOfNames", {'member': "cn=admin,{0}".format(self.base_dn)})

    def add_user(self, uid, first_name, last_name, display_name, email, password):
        ActionLogger().info(self.strings.log_add_user.format(uid))
        return self.add("uid={0},ou=users,{1}".format(uid, self.base_dn), "inetOrgPerson", {
            'displayName': display_name,
            'mail': email,
            'sn': uid,
            'givenName': first_name,
            'commonName': last_name,
            'userPassword': password
        })

    def get_users(self):
        try:
            r = self.search('ou=users,{0}'.format(self.base_dn), '(objectclass=inetOrgPerson)',
                            attributes=['cn', 'displayName', 'givenName', 'mail', 'sn', 'uid'])
            return r
        except LDAPSocketOpenError as err:
            Flash.set(Flash.TYPE_ERROR, self.strings.ldap_error)
            logging.error("{0} ({1})".format(self.strings.ldap_error, err))
            return []

    def get_user(self, uid):
        if self.base_dn in uid:
            search = uid
        else:
            search = 'uid={0},ou=users,{1}'.format(uid, self.base_dn)
        return self.search(search, '(objectclass=inetOrgPerson)', attributes=['cn', 'displayName', 'givenName', 'mail', 'sn', 'uid'])[0]

    def update_user(self, uid, first_name, last_name, display_name, email):
        ActionLogger().info(self.strings.log_update_user.format(uid))
        return self.update("uid={0},ou=users,{1}".format(uid, self.base_dn), {
            'displayName': [(MODIFY_REPLACE, [display_name])],
            'mail': [(MODIFY_REPLACE, [email])],
            'sn': [(MODIFY_REPLACE, [uid])],
            'givenName': [(MODIFY_REPLACE, [first_name])],
            'commonName': [(MODIFY_REPLACE, [last_name])],
        })

    def update_user_password(self, uid, password):
        ActionLogger().info(self.strings.log_update_user_password.format(uid))
        return self.update("uid={0},ou=users,{1}".format(uid, self.base_dn), {
            'userPassword': [(MODIFY_REPLACE, [password])],
        })

    def get_groups(self):
        try:
            r = self.search('ou=groups,{0}'.format(self.base_dn), '(objectclass=groupOfNames)', attributes=['cn', 'member'])
            return r
        except LDAPSocketOpenError as err:
            Flash.set(Flash.TYPE_ERROR, self.strings.ldap_error)
            logging.error("{0} ({1})".format(self.strings.ldap_error, err))
            return []

    def add_user_to_group(self, user, group):
        ActionLogger().info(self.strings.log_add_user_to_group.format(user, group))
        return self.update("cn={0},ou=groups,{1}".format(group, self.base_dn),
                           {'member': [MODIFY_ADD, "uid={0},ou=users,{1}".format(user, self.base_dn)]})

    def remove_user_from_group(self, user, group):
        ActionLogger().info(self.strings.log_remove_user_from_group.format(user, group))
        return self.update("cn={0},ou=groups,{1}".format(group, self.base_dn),
                           {'member': [MODIFY_DELETE, "uid={0},ou=users,{1}".format(user, self.base_dn)]})

    def delete_user(self, uid):
        ActionLogger().info(self.strings.log_delete_user.format(uid))
        return self.delete("uid={0},ou=users,{1}".format(uid, self.base_dn))

    def delete_group(self, cn):
        ActionLogger().info(self.strings.log_delete_group.format(cn))
        return self.delete("cn={0},ou=groups,{1}".format(cn, self.base_dn))


class CsvBuilder:
    def __init__(self, file):
        self.data = []
        with open(file, newline='') as csvfile:
            for row in csv.reader(csvfile):
                self.data.append(row)
            self.type = self.data[0]
            self.headers = self.data[1]
            self.data.pop(0)
            self.data.pop(0)

    def get_type(self):
        return self.type

    def get_headers(self):
        return self.headers

    def row_to_object(self, row):
        obj = {}
        for i, header in enumerate(self.headers):
            obj[header] = row[i]

        return obj

    def get_object_list(self):
        list = []
        for row in self.data:
            list.append(self.row_to_object(row))
        return list
